from scipy.optimize import minimize
import numpy as np
import scipy.sparse as sp
import sys
from matplotlib import pyplot as pp

def build_DNLS(rad,dim):
    to_flat, to_square = np.zeros((dim,dim),dtype=int), np.zeros(dim**2,dtype=int)
    count = -1
    D = np.zeros((dim,dim),dtype=int)
    for i in range(dim):
        for j in range(dim):
            if i**2+j**2 < rad**2: 
                D[i,j] = 1
                count+=1; to_flat[i,j]=count; to_square[count]=dim*i+j
    N = np.zeros((dim,dim),dtype=int)
    N[1:,:] += D[:dim-1,:]; N[:dim-1,:]+=D[1:,:]
    N[:,1:] += D[:,:dim-1]; N[:,:dim-1]+=D[:,1:]
    N[:,0] += 1
    N *= D
    S = np.zeros((dim,dim)); S[0] = 1; S = D*S
    L = np.zeros((dim,dim))
    L[1:dim-2,1:dim-2] = np.vectorize(lambda x: 1 if (x==2) or (x==3) else 0)(N[1:dim-2,1:dim-2])
    for i in range(1,dim-1): 
        if N[0,i] == 2: L[0,i] = 1
        if N[dim-1,i] == 2: L[dim-1,i] = 1
        if N[i,0] == 3: L[i,0] = 1
        if N[i,dim-1] == 2: L[i,dim-1] = 1
    if N[0,0] == 1: L[0,0] = 1
    if N[0,dim-1] == 1: L[0,dim-1] = 1
    if N[dim-1,0] == 1: L[dim-1,0] = 1
    if N[dim-1,dim-1] == 1: L[dim-1,dim-1] = 1
    return D,N,L,S,count+1,to_flat,to_square


def flatten(X,dim,dim2,to_square):
    fX = np.zeros(dim2)
    for i2 in range(dim2):
        v = to_square[i2]; i = v/dim; j = v%dim
        fX[i2] = X[i,j]
    return fX


def build_topo(dim,to_square,to_flat,D,fN,fS,Q=None,Q_fact=1):
    if Q == None: Q=np.zeros((dim,dim))
    rows, cols, data = [],[],[]
    dia = fS.copy()
    for i2 in range(fN.shape[0]):
        v = to_square[i2]; i = v/dim; j = v%dim
        iQ = Q[i,j]

        if i!=0:
            if D[i-1,j]:
                rows.append(i2); cols.append(to_flat[i-1,j])
                if iQ and Q[i-1,j]:
                    data.append(-Q_fact); dia[i2] += Q_fact
                else:
                    data.append(-1); dia[i2] += 1

        if j!=0: 
            if D[i,j-1]:
                rows.append(i2); cols.append(to_flat[i,j-1])
                if iQ and Q[i,j-1]:
                    data.append(-Q_fact); dia[i2] += Q_fact
                else:
                    data.append(-1); dia[i2] += 1

        if i!=dim-1:
            if D[i+1,j]:
                rows.append(i2); cols.append(to_flat[i+1,j])
                if iQ and Q[i+1,j]:
                    data.append(-Q_fact); dia[i2] += Q_fact
                else:
                    data.append(-1); dia[i2] += 1

        if j!=dim-1: 
            if D[i,j+1]: 
                rows.append(i2); cols.append(to_flat[i,j+1])
                if iQ and Q[i,j+1]:
                    data.append(-Q_fact); dia[i2] += Q_fact
                else:
                    data.append(-1); dia[i2] += 1

    return np.array(rows), np.array(cols), np.array(data), dia

def pdg(p,d,g,dia,coo_rows, coo_cols, coo_data):
    dia_rowsncols = np.arange(dia.shape[0])
    dia_data = g+d*dia
    M = sp.coo_matrix((np.append(dia_data,coo_data*d),(np.append(dia_rowsncols,coo_rows),np.append(dia_rowsncols,coo_cols))), shape=(dia.shape[0],dia.shape[0]))
    M = M.tocsc()
    x = sp.linalg.spsolve(M,p)
    return x

def deriv_x(px,dx,gx,x,X,rows,cols,dia):
    deriv = px*X +x*(-gx -dx*dia)
    for i in range(rows.shape[0]):
        deriv[rows[i]] += dx*x[cols[i]]
    return np.sum(deriv**2)/len(x)

def another_deriv_x(px,dx,gx,x,rows,cols,dia):
    deriv = px +x*(-gx -dx*dia)
    for i in range(rows.shape[0]):
        deriv[rows[i]] += dx*x[cols[i]]
    return deriv

def square_vect(f,dim,to_square,double=False,void=None):
    X = np.full((dim,dim),void)
    for i2 in range(f.shape[0]):
        v = to_square[i2]; i = v/dim; j = v%dim
        X[i,j] = f[i2]
    if double: return np.concatenate((np.fliplr(X),X),axis=1)
    else: return X

def optimize(f,nb_params,thr1,thr2,first_params=np.array([]),bounds=3,max_count=50):
    count = 0
    bounds = [(10**-bounds,10**bounds) for e in range(nb_params+first_params.shape[0])]
    diff = np.finfo('d').max
    while diff >= thr2:
        count += 1
        if count > max_count: sys.exit()
        diff = np.finfo('d').max
        while diff >= thr1:
            print "r", 
            params = np.append(first_params,np.random.random(nb_params)* 10.**np.random.random_integers(-2,2,nb_params))
            diff = f(params)
        params = minimize(f,params,method='L-BFGS-B',bounds=bounds).x
        diff = f(params)
    return params

levels = np.arange(0,2,0.01)

def plot_bunch(bunch,dim,to_square,save=None):
    lb = len(bunch); rows = 1
    while lb/float(rows) > rows:
        rows += 1
    cols = 1
    while rows*cols < lb:
        cols += 1
    i = 0
    pp.figure(figsize = (19,12.5))
    for v,title,relative in bunch:
        i+=1
        pp.subplot(rows,cols,i)
        pp.axis("equal")
        pp.title(title)
        if relative:
            pp.contourf(square_vect(v,dim,to_square,double=True,void=None),50,cmap="jet")
        else:
            v[np.where(v>levels[-1])]=levels[-1]
            pp.contourf(square_vect(v,dim,to_square,double=True,void=None),levels=levels,cmap="jet")
            
    if save == None: pp.show()
    else : pp.savefig(save+".png", bbox_inches='tight')

